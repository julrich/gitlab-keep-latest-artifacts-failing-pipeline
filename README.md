# gitlab-keep-latest-artifacts-failing-pipeline

This project reproduces bug https://gitlab.com/gitlab-org/gitlab/-/issues/266958 which causes artifacts of failing pipelines to be kept unnecessarily.

The issue is: until there is a successful *pipeline*, the artifacts of all previous *failing* pipelines are kept.
The expected behavior would rather be that only the artifacts of the last *successful* pipeline are kept. Maybe also those of the latest *failing* pipeline. But definitely not the artifacts of *all* failing pipelines.
